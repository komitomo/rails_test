# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RailsTest::Application.config.secret_key_base = 'd2b98107281e75891dce89224d9d5e3181a3c5f40ddb5c0ed2ab863c74b96c742202eb8f66493c28b71cff35ed0602b652c4e501eded26928576ea8f475436a5'
